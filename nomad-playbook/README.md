Consul
======

This role deploys a consul cluster (both servers and clients). Cluster should be
placed in the same group (by default `cluster`) so that configuration can be set
properly to boostrap the cluster and configure clients.

Consul is deployed as a systemd service

Feature included in consul configuration:
* possibility to configure tls
* prometheus metrics
* optional network configuration to make consul bind to internal private IP for
  non cluster traffic.
* Possibility to setup basic ACL configuration.

Requirements
------------

* Internet connectivity is required as consul binaries will be downloaded.
* Targets should use systemd and have syslog activated.

Role Variables
--------------

Roles variables are documented in default/main.yaml. Some remarks:
* `server` controls whether the consul service will be a client or a server.
* `retry_join` is the list of IPs of the cluster servers. If not configured it
  will automatically populated based on IPs from the hosts in ansible `cluster`
  group (see below to control this group name)
* `encrypt_key` is mandatory or nothing will work (use `consul keygen`)
* `tls` variables control tls activation
* when `tls` is enabled, you must provide tls certs and keys unless you activate
  `local_ca`, in that case the role will locally generate a CA and required
  certificates. They will be stored in `local_ca_dir` on host
  executing ansible.
* when `tls` is enabled, you can activate `auto_encrypt` as defined in consul so
  that clients will get their certificates directly from servers. In that case
  you don't need to provide certificates for the clients (and if `local_ca` is
  activated, generated certificates for clients won't be used).

Some internal variables are documented in vars/main.yaml. Some remarks:
* control of the ansible group name of server is controlled by `cluster_group`
  (default `cluster`)
* control of the ansible group name of clients is controlled by `clients_group`
  (default `clients`)

NOTE: you still must define `server = true` for hosts in `cluster` group.

See https://www.consul.io/docs/agent/options for consul documentation about
selected options (set via templates/consul.hcl)

Example Playbook
----------------

```
- name: gather facts
  hosts: all
  tasks:
    - name: gather facts
      debug:
        msg: gather facts
- name: setup servers
  hosts: cluster
  roles:
    - role: consul
- name: setup clients
  hosts: clients
  roles:
    - role: consul
```

Separation of the playbook in these 3 parts is important:
* you want to gather all the servers IPs addresses if you don't set `retry_join`
  so that all servers from the cluster automatically know other members. This is
  required as we do rolling update (see below).
* you want to have rolling updates (`serial: 1`) when you will use the playbook
  to update either your configuration of consul binaries.
* you want to finish with clients as they won't start unless the cluster is
  active. Here you probably want rolling updates as you probably don't expect
  all client to be down at the same time.

When each server is installed / udpated a 30s pause is included to let the
cluster elect a new leader.

For more detailed examples see molecule.yml file in molecule cluster scenarios.

ACL
---

The bootstrap of the ACL system is supported on this role. 

A master token is randomly generated and save in the path who's specified in `local_tmp_dir` variable.

By default, ACL system is enabled.

Activation of ACL system is handled by `acl_enabled` variable. You can set it to `false` in non-production environments.

Playbook with ACL non activated
-------------------------------

```
- name: CONVERGE | gather facts & finish prepare
  hosts: all
  tasks:
    - name: gather facts
      debug:
        msg: gather facts
    - name: install rsyslog
      package:
        name: rsyslog
        state: present

- name: CONVERGE | apply consul for cluster
  hosts: cluster
  serial: 1
  roles:
    - role: consul
  vars:
    acl_enabled: "false"     

- name: CONVERGE | apply consul for clients
  hosts: clients
  serial: 1
  roles:
    - role: consul
  vars:
    acl_enabled: "false"
```

Playbook with ACL activated (For production deployments)
--------------------------------------------------------
```
- name: CONVERGE | gather facts & finish prepare
  hosts: all
  tasks:
    - name: gather facts
      debug:
        msg: gather facts
    - name: install rsyslog
      package:
        name: rsyslog
        state: present

- name: CONVERGE | apply consul for cluster
  hosts: cluster
  serial: 1
  roles:
    - role: consul

- name: CONVERGE | apply consul for clients
  hosts: clients
  serial: 1
  roles:
    - role: consul

- hosts: all
  serial: 1
  tasks: 
    - name: Apply token to each Consul agent
      include_role:
        name: consul
        tasks_from: acl_register_agent
```

Molecule
--------

molecule version: 2.22
provider: lxd

Two scenarios are provided:
* `default` is designed to test platforms on which consul can be installed / this
  role will work
* `cluster` is designed to setup a 5 servers cluster and 2 clients.

New consul version
------------------

If you need to use a new consul version, update the vars/main.yml file with the
associated url and sha256 hash.

Misc
----

Access to prometheus metrics is here:

```
http[s]://<agent hostname/ip>:8500/v1/agent/metrics.format=prometheus
```
