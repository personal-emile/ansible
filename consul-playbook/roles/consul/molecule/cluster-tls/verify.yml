---
- name: Verify
  hosts: all
  serial: 1
  tasks:
  - name: Capture number of members
    shell:
      cmd: set -o pipefail; /usr/local/bin/consul members | grep alive | wc -l
      executable: /bin/bash
    changed_when: true
    register: consul_members

  - name: Check number of members
    fail:
      msg: "Wrong number of members or consul service not started"
    when: consul_members.stdout != "8"

  - name: Capture number of servers
    shell:
      cmd: set -o pipefail; /usr/local/bin/consul operator raft list-peers | grep molecule-consul-cluster | wc -l
      executable: /bin/bash
    changed_when: true
    register: consul_cluster

  - name: Check number of servers
    fail:
      msg: "Wrong number of servers or consul service not started"
    when: consul_cluster.stdout != "5"

  - name: Capture number of voters
    shell:
      cmd: set -o pipefail; /usr/local/bin/consul operator raft list-peers | grep molecule-consul-cluster | grep true | wc -l
      executable: /bin/bash
    changed_when: true
    register: consul_voter

  - name: Check number of voters
    fail:
      msg: "Wrong number of voters or consul service not started"
    when: consul_voter.stdout != "5"

  - name: Capture number of leaders
    shell:
      cmd: set -o pipefail; /usr/local/bin/consul operator raft list-peers | grep molecule-consul-cluster | grep leader | wc -l
      executable: /bin/bash
    changed_when: true
    register: consul_leader

  - name: Check number of leaders
    fail:
      msg: "Wrong number of leaders or consul service not started"
    when: consul_leader.stdout != "1"

  - name: Check prometheus metrics
    uri:
      url: http://127.0.0.1:8500/v1/agent/metrics?format=prometheus

  - name: Check RPC access in cluster
    command: /usr/local/bin/consul catalog services
    changed_when: true

  - name: Check ui access
    uri:
      url: "http://{{ ansible_default_ipv4.address }}:8500/ui"
    delegate_to: localhost
    register: rv
    failed_when: ansible_hostname == "molecule-consul-ui-tls-1" and rv.status != 200
