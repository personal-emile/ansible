# COMMON CONFIG

server = {% if server %}true{% else %}false{% endif %}

datacenter = "{{ dc_name }}"
domain = "{{ domain }}"
data_dir = "{{ data_dir }}"
encrypt = "{{ encrypt_key }}"
log_level = "{{ log_level }}"
enable_syslog = true
leave_on_terminate = true
{% if retry_join | length > 0 %}
retry_join = [ "{{ retry_join | join(',') }}" ]
{% elif cluster_group in groups %}
retry_join = [ "{{ groups[cluster_group] | map('extract', hostvars, ['ansible_default_ipv4', 'address']) | join('","') }}" ]
{% else %}
retry_join = []
{% endif %}

telemetry = {
  prometheus_retention_time = "24h"
  disable_hostname = true
}

ui = {%if ui %}true{% else %}false{% endif %}


{% if server %}
# SERVER CONFIG

{% if acl_enabled == "true" %}
acl = {
  enabled =  {{ acl_enabled }}
  default_policy = "deny"
  enable_token_persistence = true
  tokens = {
    master = "{{ consul_acl_master_token }}"
  }
}
{% endif %}

{% if retry_join | length > 0 %}
bootstrap_expect = {{ retry_join | length }}
{% else %}
bootstrap_expect = {{ groups[cluster_group] | length }}
{% endif %}
rejoin_after_leave = true
dns_config = {
  enable_truncate = true
  only_passing = true
}

{% if client_addr != "" %}
client_addr = "{{ client_addr }}"
{% else %}
client_addr = "0.0.0.0"
{% endif %}

{% if bind_addr != "" %}
bind_addr = "{{ bind_addr }}"
{% elif ansible_all_ipv4_addresses | length > 1 %}
bind_addr = "{{ ansible_default_ipv4.address }}"
{% else %}
bind_addr = "0.0.0.0"
{% endif %}

{% else %}
# CLIENT CONFIG

{% if acl_enabled == "true" %}
acl = {
  enabled =  {{ acl_enabled }}
  default_policy = "deny"
  enable_token_persistence = true
}
{% endif %}

{% if client_local %}
client_addr = "169.254.1.1"
{% elif client_addr != "" %}
client_addr = "{{ client_addr }}"
{% else %}
client_addr = "0.0.0.0"
{% endif %}

{% if bind_addr != "" %}
bind_addr = "{{ bind_addr }}"
{% elif ansible_all_ipv4_addresses | length > 1 %}
bind_addr = "{{ ansible_default_ipv4.address }}"
{% else %}
bind_addr = "0.0.0.0"
{% endif %}

{% if tls and tls_verify_incomming %}
verify_incoming = true
{% endif %}
{% if tls and tls_verify_outgoing %}
verify_outgoing = true
{% endif %}
verify_server_hostname = true
{% endif %}
