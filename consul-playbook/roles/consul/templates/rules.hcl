{% if server %}
{
  "Name": "{{policy_name}}-write",
  "Description": "Grants write access for agent and services",
  "Rules": "node \"{{ inventory_hostname }}\" { policy = \"write\"}", 
  "Datacenters": ["dc1"]
}
{% else %}
{
  "Name": "{{policy_name}}-write",
  "Description": "Grants write access for agent and services",
  "Rules": "node \"{{ inventory_hostname }}\" { policy = \"write\"}"
           "service_prefix \"flow-\" { policy = \"write\"}"
           "service_prefix \"card-\" { policy = \"write\"}"
           "service_prefix \"base-\" { policy = \"write\"}"
           "service_prefix \"core-\" { policy = \"write\"}",
  "Datacenters": ["dc1"]
}
{% endif %}