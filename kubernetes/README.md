# **Kubernetes role**
This project allows you to create a cluster Kubernetes with Kubeadm. 

Kubeadm helps you start a minimum, viable and best practice Kubernetes cluster. With kubeadm, your cluster must pass Kubernetes Conformance tests. Kubeadm also supports other life-cycle functions, such as upgrades, demotion and bootstrap tokens management.

The simplicity of kubeadm allows it to be used in a wide range of use cases:
* New users can start with kubeadm to try Kubernetes for the first time.
* Users familiar with Kubernetes can create clusters with kubeadm and test their applications.
* Larger projects may include kubeadm as a base brick in a more complex system that may also include other installation tools.

Here is what the role does:
* deploy a basic ansible.cfg
* execute prerequisites
    * disable swap
    * add IPs to /etc/hosts to link then with an hostname
* setting up nodes
    * starting and enabling docker, kubelet and firewalld
    * allows network ports in firewalld (6443 correspond to kube-apiserver and 10250 to kubelet)
    * enabling bridge firewall rule
* configure a master
    * pulling images
    * resetting kubeadm
    * initializing k8s cluster
    * storing logs & token for the nodes who join the cluster
    * copying the admin's configuration into a folder own by a non root user to allow him  to execute kubectl
    * copying the config in a terraform configuration to allow terraform to connect to the cluster
    * install network add-on flannel to allow nodes to communicate together
* configure nodes
    * copying logs & token 
    * use that to execute the command who allows nodes to join the master

## **Process**
* First, you'll have to change the “ad_addr” in the env_variables file with the IP address of the Kubernetes master node. You can also change "cidr_v" to modify the IP address range of the containers.
* Run the following command to setup the Kubernetes Master node.
```
ansible-playbook setup_master_node.yml
```
* Once the master node is ready, run the following command to set up the worker nodes.
```
ansible-playbook setup_worker_nodes.yml
```
* Once the workers have joined the cluster, run the following command to check the status of the worker nodes.
```
kubectl get nodes
```
## **Sources**
* https://kubernetes.io/fr/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/
* https://github.com/ctienshi/kubernetes-ansible
* https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
